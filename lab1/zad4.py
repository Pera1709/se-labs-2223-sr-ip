"""
Create a program that asks the user for a number and then prints out a list of all the divisors of that number
"""

num = input("Input a number: ")

try:
    num = int(num)
    divisors = []
    for i in range(1, num + 1):
        if num % i == 0:
            divisors.append(i)
    print(divisors)
    
except:
    print("Input error")
