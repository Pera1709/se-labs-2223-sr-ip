"""
Draw board
"""

def get_input(message):
    try:
        num = int(input(message))
        if num > 0:
            return num
    except:
        pass


def get_horizontal_line(width):
    res = ""
    for i in range(width * 4):
        if i % 4 != 0:
            res += "-"
        else:
            res += " "
    return res


def get_vertical_line(width):
    res = ""
    for i in range(width * 4):
        if i % 4 == 0:
            res += "|"
        else:
            res += " "
    return res + "|"


width = get_input("Width: ")
height = get_input("Height: ")

for y in range(height * 2 + 1):
    if y % 2 == 0:
        print(get_horizontal_line(width))
    else:
        print(get_vertical_line(width))
