"""
Ask the user for a number.
Depending on whether the number is even or odd, print out an appropriate message to the user
"""

num = input("Input a number: ")

try:
    if int(num) % 2 == 0:
        print("It's even")
    else:
        print("It's odd")
except:
    print("Input error")
