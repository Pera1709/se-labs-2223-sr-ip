"""
Ask the user for a string and print out whether this string is a palindrome or not.
(A palindrome is a string that reads the same forwards and backwards.)
"""

text = input("Input a string: ")

palindrome = True

for i in range(len(text)):
    if text[i] != text[len(text) - 1 - i]:
        palindrome = False
        break

if palindrome:
    print("It's a palindrome!")
else:
    print("It's not a palindrome!")
