"""
Make a two-player Rock-Paper-Scissors game.
(Hint: Ask for player plays (using input), compare them, print out a message of congratulations to the winner, and ask if the players want to start a new game)
"""

print("""
Rock     = 0
Paper    = 1
Scissors = 2
""")

lastPlayed = -1

def get_input(playerName):
    while True:
        try:
            res = int(input(playerName + ": "))
            if 0 <= res <= 2:
                return res
        except:
            pass

def get_state(first, second):
    if (first == 0 and second == 2) or (first == 1 and second == 0) or (first == 2 and second == 1):
        return -1

    if (second == 0 and first == 2) or (second == 1 and first == 0) or (second == 2 and first == 1):
        return 1

    return 0

firstPlayer = get_input("First player")
secondPlayer = get_input("Second player")

state = get_state(firstPlayer, secondPlayer)
if state == -1:
    print("First player wins!")
elif state == 1:
    print("Second player wins!")
else:
    print("Draw!")