"""
Generate a random number between 1 and 9 (including 1 and 9).
Ask the user to guess the number, then tell them whether they guessed too low, too high, or exactly right.
(Hint: remember to use the user input lessons from the very first exercise)
"""

import random

num = random.randint(0, 100)

def get_input():
    while True:
        try:
            return int(input("Your guess: "))
        except:
            pass

while True:
    val = get_input()
    
    if val < num:
        print("Too low")
    elif val > num:
        print("Too high")
    else:
        print("Correct!")
        break
