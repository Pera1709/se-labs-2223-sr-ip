from query_handler_base import QueryHandlerBase
import random
import requests
import json

class PlanetHandler(QueryHandlerBase):
    def can_process_query(self, query):
        return "planet" in query

    def process(self, query):
        planet_name = query.split()[1]
        planet_info = self.get_planet_info(planet_name)

        try:
            self.ui.say("Solar system order: " + planet_info["planetOrder"])
            self.ui.say("Mass: " + planet_info["basicDetails"][0]["mass"])
            self.ui.say("Volume: " + planet_info["basicDetails"][0]["volume"])
            self.ui.say("Description: " + planet_info["description"])
        except:
            self.ui.say('Unknown planet "%s"' % planet_name)

    def get_planet_info(self, planet_name):
        url = "https://planets-info-by-newbapi.p.rapidapi.com/api/v1/planet/list"

        headers = {
            "X-RapidAPI-Key": "0b3247b9eemsha39cd0b2bfac362p146bfdjsn78f12d42284d",
            "X-RapidAPI-Host": "planets-info-by-newbapi.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers)

        try:
            planets = json.loads(response.text)
            for planet in planets:
                if planet["name"].lower() == planet_name.lower():
                    return planet
            return {}
        except:
            return {}
