from query_handler_base import QueryHandlerBase
import random

def get_win_state(first, second):
    if (first == second):
        return 0
    if (first == 0 and second == 2) or (first == 1 and second == 0) or (first == 2 and second == 1):
        return -1
    return 1

class RockPaperScissorsHandler(QueryHandlerBase):
    possible_queries = ["rock", "paper", "scissors"]
    users_play = -1

    def can_process_query(self, query):
        for i in range(len(self.possible_queries)):
            if self.possible_queries[i] in query.lower():
                self.users_play = i
                return True
        return False

    def process(self, query):
        bots_play = random.randint(0, 2)
        self.ui.say(self.possible_queries[bots_play])

        state = get_win_state(self.users_play, bots_play)
        if state == -1:
            self.ui.say("You win!")
        elif state == 1:
            self.ui.say("I win!")
        else:
            self.ui.say("Draw!")
