def get_csv_lines(filename):
    lines = []
    try:
        with open(filename) as file:
            file.readline() # Ignoring the first line
            for line in file.readlines():
                lines.append(line.strip().replace("\n", ""))
    except:
        print("'%s' file reading error" % filename)
    return lines


def save_employee_info(filename, firstdata, seconddata):
    try:
        with open(filename, "w") as file:
            for i in range(len(firstdata)): # No array length error checking
                file.write(f"{firstdata[i]}, {seconddata[i]}\n")
    except:
        print("'%s' file saving error" % filename)


data = [[], [], [], []]

for line in get_csv_lines("lab2/ex2-text.csv"):
    parts = line.split(",")
    for i in range(len(parts)):
        data[i].append(parts[i])

save_employee_info("lab2/ex2-employees.txt", data[0], data[1])
save_employee_info("lab2/ex2-locations.txt", data[0], data[3])
