import json

def get_csv_lines(filename):
    lines = []
    try:
        with open(filename) as file:
            file.readline() # Ignoring the first line
            for line in file.readlines():
                lines.append(line.strip().replace("\n", ""))
    except:
        print("'%s' file reading error" % filename)
    return lines


lines = get_csv_lines("lab2/ex2-text.csv")

employees = []

for line in lines:
    parts = line.split(",")
    employees.append({'employee': parts[0], 'title': parts[1], 'age': parts[2], 'office': parts[3]})

with open("lab2/ex4-employees.json", "w") as file:
    json.dump(employees, file)
